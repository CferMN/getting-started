public class Main {
    /*
    This is the first comment line
    This is the second comment line
     */
    public static void main(String[] args) {
        // This is a standalone comment
        System.out.println("First line from app");
        System.out.println("Second line from app");
        System.out.println("Third line from app");
    }
}
